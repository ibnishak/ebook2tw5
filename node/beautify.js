#!/usr/bin/env node


var fs = require('fs');
var pretty = require('pretty');
var sanitizeHtml = require('sanitize-html');

const args = process.argv.slice(2)


fs.readFile(args[0], 'utf8', function(err, data) {
    if (err) throw err;
    clean = sanitizeHtml(data, {
        allowedTags: false,
        allowedAttributes: false,
        allowedSchemes: [ 'http', 'https', 'ftp', 'mailto', 'data' ],
    });
    console.log(pretty(clean));
 
});
