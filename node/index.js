#!/usr/bin/env node


var fs = require('fs');
var pretty = require('pretty');
var sanitizeHtml = require('sanitize-html');

const args = process.argv.slice(2)


fs.readFile(args[0], 'utf8', function(err, data) {
    if (err) throw err;
    clean = sanitizeHtml(data, {
	allowedTags: [ 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'blockquote', 'p', 'a', 'ul', 'ol',
		       'nl', 'li', 'b', 'i', 'strong', 'em', 'strike', 'code', 'hr', 'br', 'div', 'span', 'style', 'section',
		       'table', 'thead', 'caption', 'tbody', 'tr', 'th', 'td', 'pre', 'iframe',  ],
	allowedAttributes: {
	    a: [ 'href', 'name', 'target' ],
	    img: [ 'src' ],
	    p: ['class'],
	    div: ['class'],
	    span: ['class']
	}
    });
    console.log(pretty(clean));
});
