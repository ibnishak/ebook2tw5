#!/usr/bin/env bash

# Exit immediately if a pipeline returns non-zero.
set -o errexit

# Treat unset variables and parameters other than the special parameters ‘@’ or
# ‘*’ as an error when performing parameter expansion. An 'unbound variable'
# error message will be written to the standard error, and a non-interactive
# shell will exit.
set -o nounset


# Print a helpful message if a pipeline with non-zero exit code causes the
# script to exit as described above.
trap 'echo "Aborting due to errexit on line $LINENO. Exit code: $?" >&2' ERR


# Allow the above trap be inherited by all functions in the script.
set -o errtrace


# Return value of a pipeline is the value of the last (rightmost) command to
# exit with a non-zero status, or zero if all commands in the pipeline exit
# successfully.
set -o pipefail


# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
IFS=$'\n\t'
#===================COLORS=============================#

# Usage
# Multiline outputs like heredoc can be colores as the following
# color 123
# <code>
# coloroff
# Single line outputs are colored as _red "Hellooo"
function _color {
  printf "\e[38;5;${1}m"
}

function _coloroff {
   printf "\e[0m"
}

function _color_inline {
 printf "\n\e[%dm%s\e[0m" "$1" "$@"
}

function _red {
  printf "\n\e[31m$@\e[0m"
}
function _green {
  printf "\n\e[32m$@\e[0m"
}
function _yellow {
  printf "\n\e[33m$@\e[0m"
}
function _blue {
  printf "\n\e[34m$@\e[0m"
}
function _magenta {
  printf "\n\e[35m$@\e[0m"
}
function _cyan {
  printf "\n\e[36m$@\e[0m"
}

function _grey {
  printf "\n\e[90m$@\e[0m"
}

_yellow "Checking NodeJS"
command -v node >/dev/null 2>&1 || { echo >&2 "node not found. Aborting"; exit 1; }
_yellow "Checking Npm"
command -v npm >/dev/null 2>&1 || { echo >&2 "npm not found. Aborting"; exit 1; }
_yellow "Checking Tiddlywiki"
command -v tiddlywiki >/dev/null 2>&1 || { echo >&2 "tiddlywiki not found. Aborting"; exit 1; }
_yellow "Checking Python3"
command -v python >/dev/null 2>&1 || { echo >&2 "python not found. Aborting"; exit 1; }
_yellow "Installing node modules"
cd singlefile/cli/
npm install
cd ../../
npm install http-server -g

_green "Setup: SUCCESS\n"
