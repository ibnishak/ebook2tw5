#!/usr/bin/env bash

# Exit immediately if a pipeline returns non-zero.
set -o errexit

# Treat unset variables and parameters other than the special parameters ‘@’ or
# ‘*’ as an error when performing parameter expansion. An 'unbound variable'
# error message will be written to the standard error, and a non-interactive
# shell will exit.
set -o nounset


# Print a helpful message if a pipeline with non-zero exit code causes the
# script to exit as described above.
trap 'echo "Aborting due to errexit on line $LINENO. Exit code: $?" >&2' ERR


# Allow the above trap be inherited by all functions in the script.
set -o errtrace


# Return value of a pipeline is the value of the last (rightmost) command to
# exit with a non-zero status, or zero if all commands in the pipeline exit
# successfully.
set -o pipefail


# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
IFS=$'\n\t'

#===================COLORS=============================#

# Usage
# Multiline outputs like heredoc can be colores as the following
# color 123
# <code>
# coloroff
# Single line outputs are colored as _red "Hellooo"
function _color {
  printf "\e[38;5;${1}m"
}

function _coloroff {
   printf "\e[0m"
}

function _color_inline {
 printf "\n\e[%dm%s\e[0m" "$1" "$@"
}

function _red {
  printf "\n\e[31m$@\e[0m"
}
function _green {
  printf "\n\e[32m$@\e[0m"
}
function _yellow {
  printf "\n\e[33m$@\e[0m"
}
function _blue {
  printf "\n\e[34m$@\e[0m"
}
function _magenta {
  printf "\n\e[35m$@\e[0m"
}
function _cyan {
  printf "\n\e[36m$@\e[0m"
}

function _grey {
  printf "\n\e[90m$@\e[0m"
}


TMP="out.html"
DIR="tmpdir"
BBASE=$(basename ${1})
BNAME=$(echo "${BBASE%.*}")
SOURCEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"


NOR=$(echo "${1}" | tr ' ' _)
mv -n $1 ${NOR}

_yellow "Calibre conversion begins\n"
ebook-convert ${NOR} ebook.htmlz > /dev/null

if [ -d "${DIR}" ]; then
   rm -rf ${DIR}
fi
mkdir ${DIR}
mv ebook.htmlz ${DIR}
pushd ${DIR} > /dev/null
  _yellow "Unzipping HTMLZ\n"
  unzip -qq ebook.htmlz
  mv style.css ${SOURCEDIR}
  mv cover.jpg ${SOURCEDIR}
  _yellow "Creating Single file HTML\n"
  _red "This might take some time\n"
  #python3 -m http.server --bind="127.0.0.1" 8093 > /dev/null & 
  http-server -p 8093 -a 127.0.0.1 --silent  &
  ../singlefile/cli/single-file http://127.0.0.1:8093 ebook.html --back-end=jsdom > /dev/null
  fuser -k 8093/tcp
  sed -i 's/http\:\/\/127\.0\.0\.1\:8093\///g' ebook.html
  mv ebook.html ${SOURCEDIR}
popd > /dev/null
rm -rf ${DIR}
xdg-open ebook.html &
echo -ne "\e[38;5;9m Find the pattern marking the chapter: \e[0m"
read pat
pat=$(printf "%q" "${pat}")

node/beautify.js ebook.html > ${TMP}
_yellow "Splitting to Chapters\n"
csplit  --silent --elide-empty-files --prefix='Chapter' --suffix-format='%03d.tid' --digits 1 ${TMP} /${pat}/ '{*}'
_yellow "Building tiddlers\n"
TIDDLERS=./*.tid
TAGS="TableOfContents"
for tid in $TIDDLERS
do
    fname=$(basename ${tid})
    TITLE=$(echo "${fname%.*}")
    printf "title: %s\ntags: %s\n\n%s\n\n" "${TITLE}" "${TAGS}" "\rules except codeinline bold" > tmp
    node/beautify.js ${tid} >> tmp
    mv tmp ${tid}
done

rm ${TMP}
_yellow "Building tiddlywiki5\n"
command -v tiddlywiki >/dev/null 2>&1 || { echo >&2 "tiddlywiki not found. Aborting"; exit 1; }

if [ -d "${DIR}" ]; then
     rm -rf ${DIR}
 fi
cp -r tiddlywiki ${DIR}
mv ${TIDDLERS} ${DIR}/tiddlers
mv style.css ${DIR}/tiddlers
mv cover.jpg ${DIR}/tiddlers
printf "tags: $:/tags/Stylesheet\ntitle: Styles\ntype: text/css" > ${DIR}/tiddlers/style.css.meta
printf "tags: TableOfContents\ntitle: Cover\ntype: image/jpeg" > ${DIR}/tiddlers/cover.jpg.meta
printf "title: $:/SiteTitle\n\n%s\n" "${BNAME}" > ${DIR}/tiddlers/sitetitle.tid
printf "title: $:/SiteSubtitle\n\n " > ${DIR}/tiddlers/sitesubtitle.tid
pushd ${DIR} > /dev/null
  tiddlywiki --output ../ --build index
  RESULT=$?
  if [ $RESULT -eq 0 ]; then
      _green "Index file build: SUCCESS\n\n"
  else
      _red "Index file build: FAIL\n\n"
  fi
popd > /dev/null
rm -rf ${DIR} ebook.html
mv index.html ${BNAME}.html
